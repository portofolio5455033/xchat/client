// import logo from './logo.svg';
// import './App.css';
import React from 'react';
import Header from './Header/Header';
import Socket from './Menu Tab/Socket.io';
// import Appnavbar from './Appnavbar'
import "./App.css";

function App() {
  return (
    <div>
      <Header />
      <Socket />
    </div>
  );
}

export default App;
