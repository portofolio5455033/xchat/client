import React from 'react';
import './styles.css';
import io from 'socket.io-client';

// const socket = io.connect("http://localhost:3000");

export default function Socket() {

  return (
    <div class="app container bg-light h-100 w-50 p-0">
		<div class="screen join-screen active">
			<div class="position-absolute top-50 start-50 translate-middle">
				<h2 class="border-bottom border-primary mb-3">Join Chatroom</h2>
				<div class="form-input">
					<label class="form-label">Username</label>
					<input class="form-control mb-3" type="text" name="username" id="username"/>
					<button class="btn btn-primary" id="join-user">Join</button>
				</div>
			</div>
		</div>

		<div class="screen chat-screen ">
			<div class="navbar bg-primary ">
				<h1 class="display-6 text-light ms-4">Chatroom</h1>
				<button class="btn btn-light me-4" id="exit-chat">Exit</button>
			</div>
			<div class="messages">

			</div>
			<div class=" sticky-bottom input-group">
				<input type="text" class="form-control" placeholder="question" aria-label="question" aria-describedby="send-message" id="message-input"/>
  				<button class="btn btn-outline-primary" type="button" id="send-message">send</button>
			</div>
		</div>
	</div>
  );
};
